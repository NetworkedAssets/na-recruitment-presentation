---
@title[Introduction]

@snap[west text-25 text-bold]
NetworkedAssets
@snapend

@snap[south-west byline text-06]
Company presentation.
@snapend

---
@title[Agenda]

@snap[north-west]
The Agenda
@snapend

@snap[south-west list-content-concise span-100]
@ol
- *Our* Company
- *Your* Experience
- Quick tech questions
- Few words in English
- Formalities
- LIVE CODING
@olend
<br><br>
@snapend

---
## Our Company
@fa[arrow-down]

+++
@ol
- 2 offices - Wrocław and Berlin
- ~50 employees
- ~4 clients from Germany and UK
- domain: Telecommunication
- 4 dev teams, 1 ops team, 1 qa team
@olend

+++
## Team Presentation

+++
## CodePandas (Bear with us!)
![LOGO](img/codepandas_logo.png)

+++
### Our toolbox
- Kotlin
- SpringBoot
- JUnit 5, GherkinRunner, Mockk
- VueJS + ES6 (TS)
- Gitlab
- AWS
- docker-compose / k8s

+++
## FlatCatz
- Scala + Akka/Cats
- functional programming
- performance / concurrency

+++
## PROvisioners (Professionals and Visioners)
- Java 8/11
- Tomcat / Spring Boot

+++
## blueJays
![blueJays Logo](img/blueJays_logo.png)

+++
### Our toolbox
- Kotlin
- Spring Boot 2, JPA (Hibernate)
- JUnit 5, Mockk, AssertJ
- VueJS + ES6, Webpack, Jest
- Gitlab, Gradle

+++
@snap[north-west]
**Benefits**
@snapend

@snap[west list-content-verbose span-100]
<br>
@ul[list-bullets-circles]
- Private HealthCare (Luxmed via Saltus)
- Multisport
- Public transport ticket refund
- Fruits & Snacs
- Conference / training budget
@ulend
@snapend

+++
@snap[north-west]
**The contract**
@snapend

@snap[west list-content-verbose span-100]
<br>
@ul[list-bullets-circles]
- UoP
- No B2B possible ATM
- AKUP
- Full time required
- 3 months of probation
- Full HO during pandemic
@ulend
@snapend

---
## Your Experience
#### @fa[plus] career goals

---
## Quick tech questions
@fa[arrow-down]

+++
### Programming in general

+++
### Java & JVM

+++
### Spring & Hibernate

+++
### REST

+++
### Frontend

---
## Few words in English

---
## Formalities
@fa[arrow-down]

+++
- availbility
- contract type
- hardware & OS
- $$$

---
## LIVE CODING
- task feedback
- implementing a CR
