[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/gitpitch/the-template)

# Presentation of the NetworkedAssets Company

[gitpitch](https://gitpitch.com/NetworkedAssets/na-recruitment-presentation?grs=gitlab)

## GitPitch - The Template

*THE FASTEST WAY FROM IDEA TO PRESENTATION*

For details, see the complete template documentation [here](https://gitpitch.com/docs/the-template).

